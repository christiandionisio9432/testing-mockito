package org.cdionisio.appmockito.examples.services;

import org.cdionisio.appmockito.examples.Datos;
import org.cdionisio.appmockito.examples.models.Examen;
import org.cdionisio.appmockito.examples.repositories.ExamenRepository;
import org.cdionisio.appmockito.examples.repositories.ExamenRepositoryImpl;
import org.cdionisio.appmockito.examples.repositories.PreguntaRepository;
import org.cdionisio.appmockito.examples.repositories.PreguntaRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

// Habilita las anotaciones dependiente de mockito-junit-jupiter
@ExtendWith(MockitoExtension.class)
class ExamenServiceImplTest {

    @Mock
    ExamenRepositoryImpl repository;

    @Mock
    PreguntaRepositoryImpl preguntaRepository;

    @InjectMocks
    ExamenServiceImpl service;

    @Captor
    ArgumentCaptor<Long> captor;

//    @BeforeEach
//    void setUp() {
//        //Se habilita el uso de anotaciones para esta clase
//        MockitoAnnotations.openMocks(this);
//
////        repository = mock(ExamenRepositoryOtro.class);
////        preguntaRepository = mock(PreguntaRepository.class);
////
////        service = new ExamenServiceImpl(repository, preguntaRepository);
//    }

    @Test
    void findExamenByNombre() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);

        Optional<Examen> examen = service.findExamenByNombre("Matemáticas");

        assertTrue(examen.isPresent());
        assertEquals(5L, examen.orElseThrow().getId());
        assertEquals("Matemáticas", examen.get().getNombre());
    }

    @Test
    void findExamenByNombreEmptyList() {

        List<Examen> datos = Collections.emptyList();

        when(repository.findAll()).thenReturn(datos);

        Optional<Examen> examen = service.findExamenByNombre("Matemáticas");

        assertFalse(examen.isPresent());
    }

    @Test
    void testPreguntasExamen() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);

        Examen examen = service.findExamenByNombreWithPreguntas("Matemáticas");

        assertEquals(5, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("aritmetica"));
    }

    @Test
    void testPreguntasExamenVerify() {

        // given
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);

        // when
        Examen examen = service.findExamenByNombreWithPreguntas("Matemáticas");

        // then
        assertEquals(5, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("aritmetica"));

        verify(repository).findAll();
        verify(preguntaRepository).findPreguntasByExamenId(anyLong());
    }

    @Test
    void testNoExisteExamenVerify() {

        when(repository.findAll()).thenReturn(Collections.emptyList());
        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);

        Examen examen = service.findExamenByNombreWithPreguntas("Matemáticas2");
        assertNull(examen);

        verify(repository).findAll();
        verify(preguntaRepository).findPreguntasByExamenId(anyLong());
    }

    @Test
    void testGuardarExamen() {

        // BDD
        // Given
        Examen newExamen = Datos.EXAMEN;
        newExamen.setPreguntas(Datos.PREGUNTAS);

        when(repository.guardar(any(Examen.class))).then(new Answer<Examen>() {

            Long secuencia = 8L;

            @Override
            public Examen answer(InvocationOnMock invocationOnMock) throws Throwable {
                Examen examen = invocationOnMock.getArgument(0);
                examen.setId(secuencia++);
                return examen;
            }
        });

        // When
        Examen examen = service.guardar(newExamen);

        // then
        assertNotNull(examen.getId());
        assertEquals(8L, examen.getId());
        assertEquals("Física", examen.getNombre());

        verify(repository).guardar(any(Examen.class));
        verify(preguntaRepository).guardarVarias(anyList());
    }

    @Test
    void testManejoException() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES_ID_NULL);
        when(preguntaRepository.findPreguntasByExamenId(isNull())).thenThrow(IllegalArgumentException.class);

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            service.findExamenByNombreWithPreguntas("Matemáticas");
        });
        assertEquals(IllegalArgumentException.class, exception.getClass());

        verify(repository).findAll();
        verify(preguntaRepository).findPreguntasByExamenId(isNull());

    }

    @Test
    void testArgumentMatchers() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);

        service.findExamenByNombreWithPreguntas("Matemáticas");

        verify(repository).findAll();
//        verify(preguntaRepository).findPreguntasByExamenId(argThat(arg -> arg!=null && arg.equals(5L)));
//        verify(preguntaRepository).findPreguntasByExamenId(eq(5L));
        verify(preguntaRepository).findPreguntasByExamenId(argThat(arg -> arg!=null && arg>=5L));
    }

    @Test
    void testArgumentMatchers2() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);
        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);

        service.findExamenByNombreWithPreguntas("Matemáticas");

        verify(repository).findAll();
        verify(preguntaRepository).findPreguntasByExamenId(argThat(new MiArgsMatchers()));
    }

    public static class MiArgsMatchers implements ArgumentMatcher<Long> {

        private Long argument;

        @Override
        public boolean matches(Long aLong) {
            this.argument = aLong;
            return aLong != null && aLong > 0;
        }

        @Override
        public String toString() {
            return "es para un mensaje personalizado de error que se imprime mockito " +
                    "en caso de que falla el test " +
                    argument + " debe ser un entero positivo";
        }
    }

    @Test
    void testArgumentCaptor() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);
//        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);

        service.findExamenByNombreWithPreguntas("Matemáticas");

//        ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
        verify(preguntaRepository).findPreguntasByExamenId(captor.capture());

        assertEquals(5L, captor.getValue());
    }

    @Test
    void testDoThrow() {

        // doThrow para metodos que retornan void
        Examen examen = Datos.EXAMEN;
        examen.setPreguntas(Datos.PREGUNTAS);
        doThrow(IllegalArgumentException.class).when(preguntaRepository).guardarVarias(anyList());

        assertThrows(IllegalArgumentException.class, () -> {
            service.guardar(examen);
        });

    }

    @Test
    void testDoAnswer() {
        when(repository.findAll()).thenReturn(Datos.EXAMENES);
//        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        doAnswer(invocation -> {
            Long id = invocation.getArgument(0);
            return id == 5L? Datos.PREGUNTAS: Collections.emptyList();
        }).when(preguntaRepository).findPreguntasByExamenId(anyLong());

        Examen examen = service.findExamenByNombreWithPreguntas("Matemáticas");

        assertEquals(5, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("geometría"));
        assertEquals(5L, examen.getId());
        assertEquals("Matemáticas", examen.getNombre());

        verify(preguntaRepository).findPreguntasByExamenId(anyLong());
    }

    @Test
    void testDoAnswerGuardarExamen() {

        // BDD
        // Given
        Examen newExamen = Datos.EXAMEN;
        newExamen.setPreguntas(Datos.PREGUNTAS);

        doAnswer(new Answer<Examen>() {

            Long secuencia = 8L;

            @Override
            public Examen answer(InvocationOnMock invocationOnMock) throws Throwable {
                Examen examen = invocationOnMock.getArgument(0);
                examen.setId(secuencia++);
                return examen;
            }
        }).when(repository).guardar(any(Examen.class));

        // When
        Examen examen = service.guardar(newExamen);

        // then
        assertNotNull(examen.getId());
        assertEquals(8L, examen.getId());
        assertEquals("Física", examen.getNombre());

        verify(repository).guardar(any(Examen.class));
        verify(preguntaRepository).guardarVarias(anyList());
    }

    @Test
    void testDoCallRealMethod() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);
//        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(Datos.PREGUNTAS);
        doCallRealMethod().when(preguntaRepository).findPreguntasByExamenId(anyLong());

        Examen examen = service.findExamenByNombreWithPreguntas("Matemáticas");
        assertEquals(5L, examen.getId());
        assertEquals("Matemáticas", examen.getNombre());

    }

    @Test
    void testSpy() {

        ExamenRepository examenRepository = spy(ExamenRepositoryImpl.class);
        PreguntaRepository preguntaRepository = spy(PreguntaRepositoryImpl.class);
        ExamenService examenService = new ExamenServiceImpl(examenRepository, preguntaRepository);

        List<String> preguntas = Arrays.asList("aritmética");
//        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(preguntas);
        doReturn(preguntas).when(preguntaRepository).findPreguntasByExamenId(anyLong());

        Examen examen = examenService.findExamenByNombreWithPreguntas("Matemáticas");
        assertEquals(5, examen.getId());
        assertEquals("Matemáticas", examen.getNombre());
        assertEquals(1, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("aritmética"));

        verify(examenRepository).findAll();
        verify(preguntaRepository).findPreguntasByExamenId(anyLong());

    }

    @Test
    void testOrdenInvocaciones() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);

        service.findExamenByNombreWithPreguntas("Matemáticas");
        service.findExamenByNombreWithPreguntas("Lenguaje");

        InOrder inOrder = inOrder(preguntaRepository);
        inOrder.verify(preguntaRepository).findPreguntasByExamenId(5L);
        inOrder.verify(preguntaRepository).findPreguntasByExamenId(6L);
    }

    @Test
    void testOrdenInvocaciones2() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);

        service.findExamenByNombreWithPreguntas("Matemáticas");
        service.findExamenByNombreWithPreguntas("Lenguaje");

        InOrder inOrder = inOrder(repository, preguntaRepository);

        inOrder.verify(repository).findAll();
        inOrder.verify(preguntaRepository).findPreguntasByExamenId(5L);

        inOrder.verify(repository).findAll();
        inOrder.verify(preguntaRepository).findPreguntasByExamenId(6L);
    }

    @Test
    void testNumeroInvocaciones() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);

        service.findExamenByNombreWithPreguntas("Matemáticas");

        verify(preguntaRepository).findPreguntasByExamenId(5L);
        verify(preguntaRepository, times(1)).findPreguntasByExamenId(5L);
        verify(preguntaRepository, atLeast(1)).findPreguntasByExamenId(5L);
        verify(preguntaRepository, atLeastOnce()).findPreguntasByExamenId(5L);
        verify(preguntaRepository, atMost(10)).findPreguntasByExamenId(5L);
        verify(preguntaRepository, atMostOnce()).findPreguntasByExamenId(5L);
    }

    @Test
    void testNumeroInvocaciones2() {

        when(repository.findAll()).thenReturn(Datos.EXAMENES);

        service.findExamenByNombreWithPreguntas("Matemáticas");

//        verify(preguntaRepository).findPreguntasByExamenId(5L);
        verify(preguntaRepository, times(2)).findPreguntasByExamenId(5L);
        verify(preguntaRepository, atLeast(1)).findPreguntasByExamenId(5L);
        verify(preguntaRepository, atLeastOnce()).findPreguntasByExamenId(5L);
        verify(preguntaRepository, atMost(2)).findPreguntasByExamenId(5L);
//        verify(preguntaRepository, atMostOnce()).findPreguntasByExamenId(5L);
    }

    @Test
    void testNumeroInvocaciones3() {

        when(repository.findAll()).thenReturn(Collections.emptyList());

        service.findExamenByNombreWithPreguntas("Matemáticas");

        verify(preguntaRepository, never()).findPreguntasByExamenId(5L);
        verifyNoInteractions(preguntaRepository);

        verify(repository).findAll();
        verify(repository, times(1)).findAll();
        verify(repository, atLeast(1)).findAll();
        verify(repository, atLeastOnce()).findAll();
        verify(repository, atMost(10)).findAll();
        verify(repository, atMostOnce()).findAll();
    }
}