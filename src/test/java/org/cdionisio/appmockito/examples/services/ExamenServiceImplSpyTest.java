package org.cdionisio.appmockito.examples.services;

import org.cdionisio.appmockito.examples.Datos;
import org.cdionisio.appmockito.examples.models.Examen;
import org.cdionisio.appmockito.examples.repositories.ExamenRepository;
import org.cdionisio.appmockito.examples.repositories.ExamenRepositoryImpl;
import org.cdionisio.appmockito.examples.repositories.PreguntaRepository;
import org.cdionisio.appmockito.examples.repositories.PreguntaRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

// Habilita las anotaciones dependiente de mockito-junit-jupiter
@ExtendWith(MockitoExtension.class)
class ExamenServiceImplSpyTest {

    @Spy
    ExamenRepositoryImpl repository;

    @Spy
    PreguntaRepositoryImpl preguntaRepository;

    @InjectMocks
    ExamenServiceImpl service;

    @Test
    void testSpy() {


        List<String> preguntas = Arrays.asList("aritmética");
//        when(preguntaRepository.findPreguntasByExamenId(anyLong())).thenReturn(preguntas);
        doReturn(preguntas).when(preguntaRepository).findPreguntasByExamenId(anyLong());

        Examen examen = service.findExamenByNombreWithPreguntas("Matemáticas");
        assertEquals(5, examen.getId());
        assertEquals("Matemáticas", examen.getNombre());
        assertEquals(1, examen.getPreguntas().size());
        assertTrue(examen.getPreguntas().contains("aritmética"));

        verify(repository).findAll();
        verify(preguntaRepository).findPreguntasByExamenId(anyLong());

    }
}