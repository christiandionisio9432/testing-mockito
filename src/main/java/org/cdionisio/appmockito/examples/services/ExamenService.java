package org.cdionisio.appmockito.examples.services;

import org.cdionisio.appmockito.examples.models.Examen;

import java.util.Optional;

public interface ExamenService {

    Optional<Examen> findExamenByNombre(String nombre);

    Examen findExamenByNombreWithPreguntas(String nombre);

    Examen guardar(Examen examen);

}
