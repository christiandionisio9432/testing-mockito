package org.cdionisio.appmockito.examples.repositories;

import org.cdionisio.appmockito.examples.models.Examen;

import java.util.List;

public interface ExamenRepository {

    Examen guardar(Examen examen);
    List<Examen> findAll();
}
