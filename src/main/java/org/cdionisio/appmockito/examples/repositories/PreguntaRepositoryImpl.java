package org.cdionisio.appmockito.examples.repositories;

import org.cdionisio.appmockito.examples.Datos;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class PreguntaRepositoryImpl implements PreguntaRepository {

    @Override
    public List<String> findPreguntasByExamenId(Long id) {
        System.out.println("PreguntaRepositoryImpl.findPreguntasByExamenId");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Datos.PREGUNTAS;
    }

    @Override
    public void guardarVarias(List<String> preguntas) {
        System.out.println("PreguntaRepositoryImpl.guardarVarias");
    }
}
