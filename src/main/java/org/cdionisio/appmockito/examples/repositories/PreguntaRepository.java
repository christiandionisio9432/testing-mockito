package org.cdionisio.appmockito.examples.repositories;

import java.util.List;

public interface PreguntaRepository {

    List<String> findPreguntasByExamenId(Long id);
    void guardarVarias(List<String> preguntas);
}
